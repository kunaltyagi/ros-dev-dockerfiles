# ros-dev-dockerfiles

Dockerfiles to create dockerimages to test code compilation in ROS images.

All images contain g++-8, git and latest cmake

## Images created

Please check the [base_img.yml](base_img.yml) for the latest list.

The docker images are generated for the following flavors:
- perception
- robot
- ros-base
- ros-core

As well as the following releases:
* indigo:
    - trusty
* kinetic:
    - xenial
* lunar:
    - xenial
    - stretch
* melodic:
    - bionic
    - stretch
