#! /usr/bin/env sh

push_images() {
    prefix=${1:-kunaltyagi}
    name=${2:-melodic}
    for file in *-${name}-*.dockerfile; do
        name=`echo $file | cut -d '.' -f1`
        docker push ${prefix}/$name &
    done
    wait
}
push_images $1 $2
